import psycopg2, random, datetime, time

books = [  {'name':'grpc up and running', 'description': 'Get a comprehensive understanding of gRPC fundamentals through real-world examples. With this practical guide, you\'ll learn how this high-performance interprocess communication protocol is capable of connecting polyglot services in microservices architecture, while providing a rich framework for defining service contracts and data types'},
            {'name':'Fundamentals of Software Architecture', 'description':'This practical guide provides the first comprehensive overview of software architecture\'s many aspects. You\'ll examine architectural characteristics, architectural patterns, component determination, diagramming and presenting architecture, evolutionary architecture, and many other topics'},
            {'name':'Mastering Machine Learning Algorithms', 'description':' This newly updated and revised guide will help you master algorithms used widely in semi-supervised learning, reinforcement learning, supervised learning, and unsupervised learning domains.'},
            {'name':'Python Feature Engineering Cookbook', 'description':'Using Python libraries such as pandas, scikit-learn, Featuretools, and Feature-engine, you\'ll learn how to work with both continuous and discrete datasets and be able to transform features from unstructured datasets.'},
            {'name':'Clean Code in JavaScript', 'description':'Building robust apps starts with creating clean code. In this book, you\'ll explore techniques for doing this by learning everything from the basics of JavaScript through to the practices of clean code'},
            {'name':'Kafka Streams in Action', 'description':'Kafka Streams is a library designed to allow for easy stream processing of data flowing into a Kafka cluster. Stream processing has become one of the biggest needs for companies over the last few years as quick data insight becomes more and more important'},
            {'name':'Kafka The Definitive Guide', 'description':'Engineers from Confluent and LinkedIn who are responsible for developing Kafka explain how to deploy production Kafka clusters, write reliable event-driven microservices, and build scalable stream-processing applications with this platform'} ]

connection = psycopg2.connect(user="postgres", 
                              password="postgres",
                              host="postgres",
                              port="5432",
                              database="postgres")

def insert_order(connection, record):

  try:
    cursor = connection.cursor()

    insert_sql = """ INSERT INTO \"order\" (name, description, quantity, createAt) VALUES (%s,%s,%s,%s)"""
    cursor.execute(insert_sql, record)

    connection.commit()
    count = cursor.rowcount
    print(count, "Record inserted successfully, record: ", record[0])

  except (Exception, psycopg2.Error) as error :
    if(connection):
      print("Failed to insert record into mobile table", error)

def unix_time_millis():
  epoch = datetime.datetime.utcfromtimestamp(0)
  return (dt - epoch).total_seconds() * 1000.0

def generate_order_record(orders):
  selected_item = random.choice(orders)
  record_to_insert = (selected_item["name"], selected_item["description"], random.randrange(1, 10),  int(datetime.datetime.now().strftime("%s")) * 1000)
  return record_to_insert

if __name__ == '__main__':
 
  while True:
    record = generate_order_record(books)
    try:
      insert_order(connection, record)
      time.sleep(3)
    except (Exception, psycopg2.Error) as error :
      if(connection):
        print("Failed to insert record into mobile table", error)

  #closing database connection.
  if(connection):
    connection.close()
    print("PostgreSQL connection is closed")

  