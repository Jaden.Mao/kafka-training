#!/bin/bash

# start all services
docker-compose up -d kafka-cluster elasticsearch kibana postgres

# execute an interactive bash shell on the PostgresSQL container.
docker-compose exec postgres bash

# login into PostgresSQL
psql -d postgres -U postgres -W

#create order table
CREATE TABLE "order"(
   id SERIAL PRIMARY KEY,
   name VARCHAR NOT NULL,
   description VARCHAR,
   quantity INT,
   createAt BIGINT
);

# create JDBC source connect
{
  "name": "JdbcSourceConnector",
  "connector.class": "io.confluent.connect.jdbc.JdbcSourceConnector",
  "tasks.max": "1",
  "mode": "incrementing",
  "incrementing.column.name": "id",
  "topic.prefix": "postgres-jdbc-",
  "value.converter.schemas.enable": "true",
  "connection.url": "jdbc:postgresql://postgres/postgres?user=postgres&password=postgres",
  "value.converter": "org.apache.kafka.connect.json.JsonConverter"
}

# create Elasticsearch sink connect
{
  "name": "ElasticsearchSinkConnector",
  "connector.class": "io.confluent.connect.elasticsearch.ElasticsearchSinkConnector",
  "type.name": "kafka-connect",
  "topics": "postgres-jdbc-order",
  "tasks.max": "1",
  "topic.index.map": "\"postgres-jdbc-order:index1\"",
  "topic.key.ignore": "true",
  "value.converter.schemas.enable": "true",
  "key.ignore": "true",
  "connection.url": "http://elasticsearch:9200",
  "value.converter": "org.apache.kafka.connect.json.JsonConverter"
}

# visit Kibana 
localhost:5601

# enable index 
