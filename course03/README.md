## Kafka Streams

### WordCount app
- create topics
```bash
kafka-topics --create \
    --bootstrap-server localhost:9092 \
    --replication-factor 1 \
    --partitions 3 \
    --topic streams-plaintext-input

kafka-topics --create \
    --bootstrap-server localhost:9092 \
    --replication-factor 1 \
    --partitions 1 \
    --topic streams-wordcount-output \
    --config cleanup.policy=compact
```

- produce data 
```bash
kafka-console-producer --broker-list localhost:9092 --topic streams-plaintext-input
```

- consume data
```bash
kafka-console-consumer --bootstrap-server localhost:9092 \
    --topic streams-wordcount-output \
    --from-beginning \
    --formatter kafka.tools.DefaultMessageFormatter \
    --property print.key=true \
    --property print.value=true \
    --property key.deserializer=org.apache.kafka.common.serialization.StringDeserializer \
    --property value.deserializer=org.apache.kafka.common.serialization.LongDeserializer
```

### Lab env
- Kafka broker: 3.90.146.136:9092
- [Kafka UI](http://3.90.146.136:9000/)
