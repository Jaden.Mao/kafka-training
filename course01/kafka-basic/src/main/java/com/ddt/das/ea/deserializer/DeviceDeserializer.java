package com.ddt.das.ea.deserializer;

import com.ddt.das.ea.model.Device;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.common.serialization.Deserializer;

import java.io.IOException;
import java.util.Map;

@Slf4j
public class DeviceDeserializer implements Deserializer {

    @Override
    public void configure(Map configs, boolean isKey) {

    }

    @Override
    public Object deserialize(String topic, byte[] data) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.readValue(data, Device.class);
        } catch (IOException e) {
            log.info("Deserialize error!", e);
        }
        return null;
    }

    @Override
    public void close() {

    }
}
