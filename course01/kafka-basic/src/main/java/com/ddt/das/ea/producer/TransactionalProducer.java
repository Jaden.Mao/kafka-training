package com.ddt.das.ea.producer;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.KafkaException;

import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

import static org.apache.kafka.clients.consumer.ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG;
import static org.apache.kafka.clients.producer.ProducerConfig.ENABLE_IDEMPOTENCE_CONFIG;
import static org.apache.kafka.clients.producer.ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG;
import static org.apache.kafka.clients.producer.ProducerConfig.TRANSACTIONAL_ID_CONFIG;
import static org.apache.kafka.clients.producer.ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG;

@Slf4j
public class TransactionalProducer {

    public static void main(String[] args) throws InterruptedException {
        KafkaProducer<String, String> producer = createKafkaProducer();

        producer.initTransactions();
        try{
            producer.beginTransaction();

            sendMessages(producer);
//            sendMessagesAndTriggerException(producer);
//            sendDelayedMessages(producer);

            producer.commitTransaction();
            log.info("transaction committed successfully!");
        }catch (KafkaException e){
            producer.abortTransaction();
            log.error("push data failed!", e);
        }
    }

    private static void sendMessages(KafkaProducer<String, String> producer) {
        Stream.of("record1", "record2", "record3").forEach(
                s -> producer.send(new ProducerRecord<String, String>("input", null, s)));
    }

    private static void sendDelayedMessages(KafkaProducer<String, String> producer) throws InterruptedException {
        producer.send(new ProducerRecord<String, String>("input", null, "record1"));
        TimeUnit.SECONDS.sleep(5);
        producer.send(new ProducerRecord<String, String>("input", null, "record2"));
        TimeUnit.SECONDS.sleep(5);
        producer.send(new ProducerRecord<String, String>("input", null, "record3"));
    }

    private static void sendMessagesAndTriggerException(KafkaProducer<String, String> producer) throws InterruptedException {
        producer.send(new ProducerRecord<String, String>("input", null, "record1"));
        producer.send(new ProducerRecord<String, String>("input", null, "record2"));
        producer.send(new ProducerRecord<String, String>("input", null, "record3"));
        TimeUnit.SECONDS.sleep(5);
        throw new KafkaException("Broker not found!");
    }

    private static KafkaProducer<String, String> createKafkaProducer() {
        Properties props = new Properties();
        props.put(BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        props.put(ENABLE_IDEMPOTENCE_CONFIG, "true");
        props.put(TRANSACTIONAL_ID_CONFIG, "prod-0");
        props.put(KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
        props.put(VALUE_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");

        return new KafkaProducer(props);

    }
}
