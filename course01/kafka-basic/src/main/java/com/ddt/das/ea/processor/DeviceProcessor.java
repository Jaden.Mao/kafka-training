package com.ddt.das.ea.processor;

import com.ddt.das.ea.deserializer.DeviceSerializer;
import com.ddt.das.ea.model.Device;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import static java.time.Duration.ofSeconds;
import static org.apache.kafka.clients.CommonClientConfigs.BOOTSTRAP_SERVERS_CONFIG;
import static org.apache.kafka.clients.consumer.ConsumerConfig.*;
import static org.apache.kafka.clients.producer.ProducerConfig.*;

@Slf4j
public class DeviceProcessor {

    public static final BigDecimal TEMPERATURE_THRESHOLD = new BigDecimal("30");
    public static final String ABNORMAL_DEVICE_TOPIC = "streaming.iot.device.abnormal";
    public static final String DEVICE_TOPIC = "streaming.iot.device";

    public static void main(String[] args) throws InterruptedException {
        Properties props = createKafkaConsumer();

        Consumer<String, Device> consumer = new KafkaConsumer<>(props);
        consumer.subscribe(Arrays.asList(DEVICE_TOPIC));

        ObjectMapper mapper = new ObjectMapper();
        KafkaProducer<String, Device> producer = createKafkaProducer();

        try {
            log.info("Start listen incoming messages ...");

            while (true) {
                // poll records from Kafka
                ConsumerRecords<String, Device> records = consumer.poll(ofSeconds(5));

                for (ConsumerRecord<String, Device> record : records){
                    log.info("Device received: {}", record.value());
                    Device srcDevice = record.value();
                    double temperatureF = toFahrenheit(srcDevice.getTemperature());
                    double temperatureC = fahrenheitToCelsius(temperatureF);
                    if(isTemperatureAbnormal(temperatureC)) {
                        Device abnormalDevice = new Device (srcDevice.getId(), Double.toString(temperatureC), srcDevice.getTimestamp());
                        log.info("abnormalDevice: {}", abnormalDevice);
                        // write abnormal device to Kafka
                        producer.send(new ProducerRecord<>(ABNORMAL_DEVICE_TOPIC,
                                Long.toString(abnormalDevice.getId()), abnormalDevice));
                        TimeUnit.SECONDS.sleep(2);
                    }

                }
            }
        } finally {
            consumer.close();
            log.info("Stop listen incoming messages");
        }
    }

    private static Properties createKafkaConsumer() {
        Properties props = new Properties();
        props.put(BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        props.put(GROUP_ID_CONFIG, "jsonConsumerGroup2"); // ConsumerGroup
        props.put(KEY_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");
        props.put(VALUE_DESERIALIZER_CLASS_CONFIG, "com.ddt.das.ea.deserializer.DeviceDeserializer");
        props.put(AUTO_OFFSET_RESET_CONFIG, "earliest");
        return props;
    }

    private static KafkaProducer<String, Device> createKafkaProducer() {
        Properties props = new Properties();
        props.put(BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        props.put(KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
        props.put(VALUE_SERIALIZER_CLASS_CONFIG, DeviceSerializer.class.getName());
        props.put(ACKS_CONFIG, "1");

        return new KafkaProducer(props);

    }

    private static double toFahrenheit(String temperature) {
        if(temperature == null && temperature.equals("")) {
            return 0;
        }
        String temperatureTrim = temperature.trim().replace("F", "");
        return Double.parseDouble(temperatureTrim);
    }

    private static double fahrenheitToCelsius(double fahrenheit) {
        return new BigDecimal((( 5 *(fahrenheit - 32.0)) / 9.0))
                .setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
    }

    private static boolean isTemperatureAbnormal(double temperatureC) {
        return new BigDecimal(temperatureC).compareTo(TEMPERATURE_THRESHOLD) == 1;
    }
}
