package com.ddt.das.ea.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;
import java.util.Random;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Device {
    private long id;
    private String temperature;
    private long timestamp;
}
